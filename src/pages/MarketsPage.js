import { IconsViewSwitcher, Navbar } from "../components/BrokerDashboard/index";
import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import BondTiersList from "../components/BondTiersList";
import MarketsList from "../components/MarkketsList";
import OTCDesksList from "../components/OTCDesksList";
import { TabsContext } from "../context/TabsContext";

import MainLayout from "../layouts/MainLayout";

import { ReactComponent as IconClose } from "../static/images/clipIcons/close.svg";
import { ReactComponent as RefreshIcn } from "../static/images/clipIcons/refresh.svg";

const tabs = ["Investment Coins", "OTCDesks", "Bond Tiers"];

function MarketsPage({ hide }) {
  const history = useHistory();
  const { navTabs } = useContext(TabsContext);

  const [list, setList] = useState(false);
  const [refresh, setRefresh] = useState(false);

  const [search, setSearch] = useState("");

  const [searchOn, setSearchOn] = useState(false);

  const [tabClick, setTabClick] = useState(false);
  const [tabSelected, setTabSelected] = useState("Investment Coins");

  const getTabContent = () => {
    switch (tabSelected) {
      case "Bond Tiers":
        return (
          <BondTiersList setList={setList} list={list} searchOn={searchOn} />
        );
      case "OTCDesks":
        return (
          <OTCDesksList setList={setList} list={list} searchOn={searchOn} />
        );
      default:
        return (
          <MarketsList setList={setList} list={list} searchOn={searchOn} />
        );
    }
  };

  return (
    <MainLayout active={"Markets"} hide={hide}>
      <Navbar
        navTabs={navTabs}
        tabs={tabs}
        tabSelected={tabSelected}
        setTabSelected={(tab) => setTabSelected(tab)}
        search={search}
        setSearch={setSearch}
        viewSwitcher={
          <IconsViewSwitcher
            listIcn={list && <IconClose />}
            boardIcn={<RefreshIcn />}
            board={refresh}
            onBoardClick={() => {
              if (refresh) setRefresh(false);
              else {
                setRefresh(true);
                setList(false);
              }
            }}
            list={list}
            onListClick={() => {
              if (list) setList(false);
              else {
                setRefresh(false);
              }
            }}
            search={searchOn}
            onSearchClick={() => setSearchOn(!searchOn)}
          />
        }
        tabClick={() => {
          setTabClick(!tabClick);
        }}
        searchPlaceHolder=""
      />
      <div className="marketsView">{getTabContent()}</div>
    </MainLayout>
  );
}

export default MarketsPage;
