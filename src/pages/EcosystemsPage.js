import { IconsViewSwitcher } from "../components/BrokerDashboard/index";
import React, { useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from "react-router-dom";

import MainLayout from "../layouts/MainLayout";
import NavbarWithdrawals from "../layouts/Navbar/NavbarWithdrawals";

import { ReactComponent as IconAssets } from "../static/images/sidebarIcons/assetsLogo.svg";
import { ReactComponent as IconCounsel } from "../static/images/sidebarIcons/counsel.svg";
import { ReactComponent as IconTeamforce } from "../static/images/sidebarIcons/teamforce.svg";

const tabs = ["Ecosystems"];

function EcosystemsPage({ hide }) {
  const history = useHistory();
  const [dropDownOpen, setDropDownOpen] = useState(false);
  const [search, setSearch] = useState("");
  const [selectedBanker, setSelectedBanker] = useState({});

  const [isList, setIsList] = useState(true);
  const [searchOn, setSearchOn] = useState(false);

  const [tabClick, setTabClick] = useState(false);
  const [tabSelected, setTabSelected] = useState("Ecosystems");

  return (
    <MainLayout active={"Ecosystems"} hide={hide}>
      <NavbarWithdrawals
        tabs={tabs}
        changeTab={tabSelected}
        onChangeTab={(tab) => setTabSelected(tab)}
        onNew={() => {}}
        dropDownOpen={dropDownOpen}
        search={search}
        setSearch={setSearch}
        viewSwitcher={
          <IconsViewSwitcher
            list={isList}
            board={false}
            search={searchOn}
            onSearchClick={() => setSearchOn(!searchOn)}
            onListClick={() => setIsList(true)}
            onBoardClick={() => setIsList(false)}
          />
        }
        button={
          <div
            className={`publication ${dropDownOpen}`}
            onClick={() => setDropDownOpen(!dropDownOpen)}
          >
            {dropDownOpen ? (
              <span>Bankers</span>
            ) : (
              <>
                <img
                  className="logoIcn"
                  src={selectedBanker && selectedBanker.profilePicURL}
                  alt=""
                />
                <span>{selectedBanker && selectedBanker.bankerTag}</span>
              </>
            )}
          </div>
        }
        tabClick={() => setTabClick(!tabClick)}
      />
      {searchOn && (
        <div className="viewingAs">
          <input
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            type="text"
            className="searchInp"
            placeholder="Search Ecosystems"
          />
          <div className={`btnType `}>All</div>
          <div className={`btnType `}>Filter Config</div>
        </div>
      )}
      <div className="justiceContent">
        <Scrollbars className="justiceListWrap">
          <div className="listItem" onClick={() => history.push("/AssetsIo")}>
            <span className="eco asset">
              <IconAssets />
              <span>Assets</span>
            </span>
          </div>
          <div className="listItem" onClick={() => history.push("/Justice")}>
            <span className="eco justice">
              <IconCounsel />
              <span>Justice</span>
            </span>
          </div>
          <div className="listItem" onClick={() => history.push("/Teamforce")}>
            <span className="eco tf">
              <IconTeamforce />
              <span>Teamforce</span>
            </span>
          </div>
        </Scrollbars>
      </div>
    </MainLayout>
  );
}

export default EcosystemsPage;
