import React from "react";
import { Link } from "react-router-dom";
import vaultLogo from "../static/images/logos/civilisationIcon.svg";
function LandingPage() {
  return (
    <div className="landingPage">
      <div className="logoName">
        <img src={vaultLogo} className="logo" alt="" /> VAULT
      </div>
      <Link to="/sp" className="btnOnGod">
        Print Receipt
      </Link>
    </div>
  );
}

export default LandingPage;
