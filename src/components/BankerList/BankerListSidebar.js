import Axios from "axios";
import React, { useContext, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import { MainContext } from "../../context/MainContext";
import pathIcn from "../../static/images/clipIcons/pathData.svg";
import editApp from "../../static/images/clipIcons/appData.svg";
import deleteApp from "../../static/images/refreshIcon/delete.svg";
import yesIcn from "../../static/images/clipIcons/yes.svg";
import noIcn from "../../static/images/clipIcons/no.svg";
import back from "../../static/images/back.svg";
import close from "../../static/images/close.svg";
import LoadingAnim from "../LoadingAnim/LoadingAnim";
import { useHistory } from "react-router-dom";
import EditBanker from "./EditBanker";

function BankerListSidebar({ xchangeSelected, onClose, inTab, setInTab }) {
  const { email, token, tostShowOn } = useContext(MainContext);
  const [tab, setTab] = useState("Actions");
  const [step, setStep] = useState("");
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  function deleteBanker() {
    setLoading(true);
    Axios.post(
      "https://teller2.apimachine.com/admin/lxuser/delete/banker", // additional context
      {
        email: xchangeSelected?.email,
      },
      {
        headers: {
          email,
          token,
        },
      }
    )
      .then(({ data }) => {
        if (data.status) {
          setStep("deleteSucces");
        } else {
          tostShowOn(data.message || "Something Went Wrong");
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }
  function getContent() {
    switch (true) {
      case tab === "Actions" && step === "Edit":
        return <EditBanker banker={xchangeSelected} />;
      case tab === "Actions" && step === "deleteConfirm":
        return (
          <>
            <div className="godsEyeFilterCurrency">
              <div className="labelItm">
                Are You Sure You Want To Delete {xchangeSelected?.displayName}?
              </div>
              <Scrollbars className="coinList">
                <div className="coinItem" onClick={() => deleteBanker()}>
                  <img src={yesIcn} alt="" className="coin" />
                  <span>Confirm</span>
                </div>
                <div className="coinItem" onClick={() => setStep("")}>
                  <img src={noIcn} alt="" className="coin" />
                  <span>No</span>
                </div>
              </Scrollbars>
            </div>
          </>
        );
      case tab === "Actions" && step === "deleteSucces":
        setTimeout(() => {
          history.push("/society/Bankers");
          window.location.reload();
        }, 2000);
        return (
          <>
            <div className="godsEyeFilterCurrency">
              <div className="labelItm m-auto">
                You Have Successfully Deleted Banker{" "}
                {xchangeSelected?.displayName}. You Will Be Redirected To The
                Banker List Right Now
              </div>
            </div>
            <div className="footerBtns">
              <div
                className="btnReset"
                onClick={() => {
                  try {
                    onClose();
                  } catch (error) {}
                }}
              >
                Close
              </div>
              <div className="btnSave">Share</div>
            </div>
          </>
        );
      case tab === "Actions":
        return (
          <>
            <div className="godsEyeFilterCurrency">
              <div className="labelItm">
                What Do You Want To Do For This Banker Account?
              </div>
              <Scrollbars className="coinList">
                <div className="coinItem " onClick={() => setStep("Edit")}>
                  <img src={editApp} alt="" className="coin" />
                  <span>Edit Banker Profile</span>
                </div>
                <div className="coinItem disable" onClick={() => {}}>
                  <img src={pathIcn} alt="" className="coin" />
                  <span>See Payment Paths</span>
                </div>
                <div
                  className="coinItem"
                  onClick={() => setStep("deleteConfirm")}
                >
                  <img src={deleteApp} alt="" className="coin" />
                  <span>Delete Banker</span>
                </div>
              </Scrollbars>
            </div>
          </>
        );
      default:
    }
  }
  return (
    <div
      className="godsEyeUserFiter"
      style={{ flex: "0 0 550px", maxWidth: 550 }}
    >
      <div className="headTabBox">
        <div
          className={`tab ${tab === "About"}`}
          onClick={() => {
            setTab("About");
            setStep("");
          }}
        >
          About
        </div>
        <div
          className={`tab ${tab === "Actions"}`}
          onClick={() => {
            setTab("Actions");
            setStep("");
          }}
        >
          Actions
        </div>
      </div>
      <div className="header">
        <div className="content">
          <div className="title">
            <img src={xchangeSelected?.profilePicURL} alt="" className="icon" />
            {xchangeSelected?.displayName}
          </div>
          <div className="breadCrumbs">
            <span
              onClick={() => {
                setStep("");
              }}
            >
              {tab === "Actions"
                ? `${xchangeSelected?.displayName} Actions`
                : `About ${xchangeSelected?.displayName}`}
            </span>{" "}
            {step ? (
              <>
                -&gt;&nbsp;
                <span>{step}</span>
              </>
            ) : (
              ""
            )}
          </div>
        </div>
        {step ? (
          <div
            className="backBtn"
            onClick={() => {
              setStep("");
            }}
          >
            <img src={back} alt="" />
          </div>
        ) : (
          ""
        )}
        <div
          className="backBtn"
          onClick={() => {
            onClose();
          }}
        >
          <img src={close} alt="" />
        </div>
      </div>
      {getContent()}
      {loading && <LoadingAnim />}
    </div>
  );
}

export default BankerListSidebar;
