import React, { useRef } from "react";
import OnOutsideClick from "../../utils/OnOutsideClick";

function PublicationItem({ publication, onClick }) {
  const btnRef = useRef();
  const outRef = useRef();

  OnOutsideClick(
    btnRef,
    () => {
      try {
        onClick();
      } catch (error) {}
    },
    outRef
  );

  return (
    <div className="listItem" ref={outRef}>
      <div className="profile">
        <img src={publication?.profile_pic} alt="" className="icon" />
        <span className="name">{publication?.name}</span>
      </div>
      <span className="count">{publication?.country}</span>
      <div
        className="btnWebsite"
        ref={btnRef}
        onClick={() =>
          window
            .open(
              publication?.website.includes("http")
                ? publication?.website
                : `https://${publication?.website}`,
              "_blank"
            )
            .focus()
        }
      >
        Website
      </div>
    </div>
  );
}

export default PublicationItem;
