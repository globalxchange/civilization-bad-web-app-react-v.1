import React, { Fragment, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import { useOperatorsList } from "../../queryHooks";

function OperatorList({ setOperator, onClose }) {
  const { data: operators, isLoading: operatorLoading } = useOperatorsList();
  const [search, setSearch] = useState("");
  return (
    <Fragment>
      <div className="titleOp">Select GXLive Operator</div>
      <div className="searchWrap">
        <input
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          type="text"
          placeholder="Search Operator....|"
        />
      </div>
      <Scrollbars className="searchList">
        {operatorLoading
          ? Array(6)
              .fill("")
              .map((_, i) => (
                <div className="user" key={i}>
                  <Skeleton className="dp" circle />
                  <div className="userDetail">
                    <Skeleton className="name" width={200} />
                    <Skeleton className="email" width={200} />
                  </div>
                </div>
              ))
          : operators
              .filter(
                (operator) =>
                  operator.brand_name
                    .toLowerCase()
                    .includes(search.toLowerCase()) ||
                  operator.email.toLowerCase().includes(search.toLowerCase())
              )
              .map((operator) => (
                <div
                  className="user"
                  key={operator._id}
                  onClick={() => {
                    setOperator(operator);
                    onClose();
                  }}
                >
                  <img className="dp" src={operator.brand_logo} alt="" />
                  <div className="userDetail">
                    <div className="name">{operator.brand_name}</div>
                    <div className="email">{operator.email}</div>
                  </div>
                </div>
              ))}
        <div className="space"></div>
      </Scrollbars>
      <div className="ftBtns">
        <div className="newField" onClick={() => onClose()}>
          Go Back
        </div>
        <div className="btnSubmit" onClick={() => {}}>
          Submit
        </div>
      </div>
    </Fragment>
  );
}

export default OperatorList;
