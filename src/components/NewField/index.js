import React from "react";

function NewField(value, index, setValue) {
  return (
    <>
      <div className="name">Enter Key {index + 1}</div>
      <div className="inputWrap">
        <input
          type="text"
          className="text"
          placeholder="Enter Code Here..."
          value={value[index].key}
          onChange={(e) =>
            setValue((value) => {
              const tmp = [...value];
              tmp[index].key = e.target.value;
              return tmp;
            })
          }
        />
      </div>
      <div className="name">Enter Value {index + 1}</div>
      <div className="inputWrap">
        <input
          type="text"
          className="text"
          placeholder="Enter Code Here..."
          value={value[index].value}
          onChange={(e) =>
            setValue((value) => {
              const tmp = [...value];
              tmp[index].value = e.target.value;
              return tmp;
            })
          }
        />
      </div>
    </>
  );
}

export default NewField;
