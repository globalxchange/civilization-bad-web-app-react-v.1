import { SearchAndFilter } from "../BrokerDashboard/index";
import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import MallsItem from "./MallsItem";
import MallListSidebar from "./MallListSidebar";
import { MoreTabContext } from "../../context/MoreTabContext";

function MallsList({ list, setList, searchOn }) {
  const [endorsement, setEndorsement] = useState();
  const { malllist, malllistLoading } = useContext(MoreTabContext);

  useEffect(() => {
    if (endorsement) setList(true);
    else setList(false);
  }, [endorsement, setList]);

  useEffect(() => {
    if (!list) setEndorsement();
  }, [list]);

  // Searc Vars

  const [search, setSearch] = useState("");
  const [isFilter, setIsFilter] = useState(false);
  const [currentPage, setCurrentPage] = useState(false);
  const [filteEndorsementName, setFilteEndorsementName] = useState("");

  return (
    <>
      {
        <div className="godsEyeMoreList">
          {searchOn && (
            <SearchAndFilter
              search={search}
              setSearch={setSearch}
              placeholder={"Search Malls"}
              filterBy={`${"Name Of Group"}`}
              mainList={[
                {
                  key: "0",
                  label: "Function Performed By Search Bar",
                  switch: isFilter,
                  switchLabel: isFilter ? "Filter" : "Search",
                  switchClick: () => setIsFilter(!isFilter),
                },
                {
                  key: "1",
                  label: "Filter Range",
                  switch: currentPage,
                  switchLabel: currentPage ? "Current Page" : "Entire List",
                  switchClick: () => setCurrentPage(!currentPage),
                },
              ]}
              filterList={[
                {
                  key: "11",
                  label: "Name Of Market",
                  switch: filteEndorsementName,
                  switchLabel: filteEndorsementName ? "On" : "Off",
                  switchClick: () =>
                    setFilteEndorsementName(!filteEndorsementName),
                },
              ]}
            />
          )}
          <Scrollbars className="moreListWrapper">
            {malllistLoading
              ? Array(6)
                  .fill("")
                  .map((_, i) => (
                    <div className="listItem">
                      <Skeleton alt="" className="icon" />
                      <Skeleton className="name" width={400} />
                      <Skeleton className="count" width={30} />
                    </div>
                  ))
              : malllist
                  .filter((data) =>
                    data.name.toLowerCase().includes(search.toLowerCase())
                  )
                  .map((data) => (
                    <MallsItem
                      key={data._id}
                      endorsement={data}
                      onClick={() => setEndorsement(data)}
                      selected={endorsement}
                    />
                  ))}
          </Scrollbars>
        </div>
      }
      {endorsement && (
        <MallListSidebar
          onClose={() => setEndorsement()}
          endorsement={endorsement}
        />
      )}
    </>
  );
}

export default MallsList;
