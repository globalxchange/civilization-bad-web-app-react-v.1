import { SearchAndFilter } from "../BrokerDashboard/index";
import Axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import FounderListItem from "./FounderListItem";

import FounderListSidebar from "./FounderListSidebar";

function FoundersList({ searchOn, list, setList }) {
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);
  const [operatorList, setOperatorList] = useState([]);
  const [count, setCount] = useState(8);
  const [founderSelected, setFounderSelected] = useState();
  const ref = useRef();
  useEffect(() => {
    if (searchOn) {
      setTimeout(() => {
        ref?.current && ref.current.focus();
      }, 200);
    }
  }, [searchOn]);
  useEffect(() => {
    setLoading(true);
    Axios.get("https://comms.globalxchange.io/gxb/app/gxlive/user/operator/get")
      .then(({ data }) => {
        if (data.status) {
          setOperatorList(data.operators);
        }
      })
      .finally(() => setLoading(false));
  }, []);
  const [tab, setTab] = useState("");

  useEffect(() => {
    if (founderSelected) setList(true);
    else setList(false);
  }, [founderSelected, setList]);

  useEffect(() => {
    if (!list) setFounderSelected();
  }, [list]);

  // Filter Variables
  const [currentPage, setCurrentPage] = useState(true);
  const [isFilter, setIsFilter] = useState(false);
  const [filterBrandName, setFilterBrandName] = useState(true);
  const [filterEmail, setFilterEmail] = useState(false);
  const [filterCountry, setFilterCountry] = useState(false);

  return (
    <>
      <div className="bankerList">
        {searchOn && (
          <SearchAndFilter
            search={search}
            setSearch={setSearch}
            placeholder="Type In Any Bankers....."
            filterBy={`${filterEmail ? "Email ," : ""}${
              filterBrandName ? "Brand Name ," : ""
            }${filterCountry ? "Country" : ""}`}
            mainList={[
              {
                key: "0",
                label: "Function Performed By Search Bar",
                switch: isFilter,
                switchLabel: isFilter ? "Filter" : "Search",
                switchClick: () => setIsFilter(!isFilter),
              },
              {
                key: "1",
                label: "Filter Range",
                switch: false,
                switchLabel: currentPage ? "Current Page" : "Entire List",
                switchClick: () => setCurrentPage(!currentPage),
              },
            ]}
            filterList={[
              {
                key: "10",
                label: "Brand Name",
                switch: filterBrandName,
                switchLabel: filterBrandName ? "On" : "Off",
                switchClick: () => setFilterBrandName(!filterBrandName),
              },
              {
                key: "11",
                label: "Email",
                switch: filterEmail,
                switchLabel: filterEmail ? "On" : "Off",
                switchClick: () => setFilterEmail(!filterEmail),
              },
              {
                key: "12",
                label: "Country",
                switch: filterCountry,
                switchLabel: filterCountry ? "On" : "Off",
                switchClick: () => setFilterCountry(!filterCountry),
              },
            ]}
          />
        )}
        <Scrollbars
          autoHide
          className="bankerListWrapper"
          renderView={(props) => (
            <div
              {...props}
              className={`xchangeList ${Boolean(founderSelected)}`}
            />
          )}
          onScrollFrame={(data) => {
            data.top === 1 && setCount((count) => count + 10);
          }}
        >
          {loading
            ? Array.from(Array(10).keys()).map((key) => (
                <div className="userItem" key={key}>
                  <Skeleton circle width={50} height={50} />
                  <div className="nameEmail">
                    <span className="name">
                      <Skeleton />
                    </span>
                    <span className="email">
                      <Skeleton />
                    </span>
                  </div>
                  <div className="time">
                    <Skeleton />
                  </div>
                  <div className="balance">
                    <Skeleton />
                  </div>
                  <div className="balance">
                    <Skeleton />
                  </div>
                  <div className="btnActions">
                    <Skeleton className="btnAction" />
                  </div>
                </div>
              ))
            : operatorList
                .filter(
                  (user) =>
                    (filterBrandName &&
                      user?.brand_name
                        ?.toLowerCase()
                        .includes(search.toLowerCase())) ||
                    (filterEmail &&
                      user?.email
                        ?.toLowerCase()
                        .includes(search.toLowerCase())) ||
                    (filterCountry &&
                      user?.brand_country
                        ?.toLowerCase()
                        .includes(search.toLowerCase()))
                )
                .slice(0, count)
                .map((user) => (
                  <FounderListItem
                    key={user._id}
                    founderSelected={founderSelected}
                    setFounderSelected={setFounderSelected}
                    user={user}
                    setTab={setTab}
                  />
                ))}
          <div
            className="fxClose"
            onClick={() => {
              setList(false);
              setFounderSelected();
            }}
          />
        </Scrollbars>
      </div>
      {founderSelected && (
        <FounderListSidebar
          founderSelected={founderSelected}
          onClose={() => setFounderSelected()}
          setInTab={setTab}
          inTab={tab}
        />
      )}
    </>
  );
}
export default FoundersList;
