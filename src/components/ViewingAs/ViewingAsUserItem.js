import Axios from "axios";
import React, { useEffect, useState } from "react";
import guest from "../../static/images//guest.jpg";

function ViewingAsUserItem({ user, onClick, onCopy, onExpand }) {
  const [userData, setUserData] = useState({});
  useEffect(() => {
    user &&
      Axios.get(
        `https://comms.globalxchange.io/user/details/get?email=${user.email}`
      ).then(({ data }) => {
        if (data.status) {
          setUserData(data.user);
        }
      });
  }, [user]);
  return (
    <div
      onClick={() => {
        try {
          onClick();
        } catch (error) {}
      }}
      className="userItem"
    >
      <img
        src={(user && user.profile_img) || guest}
        alt=""
        className="profileImg"
      />
      <div className="name">{user.name}</div>
      <div className="email">{user.email}</div>
      <div className="nameEmail">
        {user.name} | {user.email}
      </div>
      <div className="btnActions">
        <div
          className="btnAction"
          onClick={() => {
            try {
              onCopy(userData);
            } catch (error) {}
          }}
        >
          Copy
        </div>
        <div
          className="btnAction"
          onClick={() => {
            try {
              onExpand(userData);
            } catch (error) {}
          }}
        >
          Expand
        </div>
      </div>
    </div>
  );
}

export default ViewingAsUserItem;
