import React, { useEffect, useState } from "react";
import Axios from "axios";
import Scrollbars from "react-custom-scrollbars";

import allImg from "../../static/images/coinIcon/all.svg";

function ViweingAsWithdrawals({
  emailSearch,
  setEmailSearch,
  setAppCode,
  setCoinSymbol,
  setMoreFilter,
  placeholder,
}) {
  const [showList, setShowList] = useState("");
  const [appSelected, setAppSelected] = useState("");
  const [coinSelected, setCoinSelected] = useState("");

  const [coinList, setCoinList] = useState([]);
  useEffect(() => {
    Axios.post("https://comms.globalxchange.io/coin/vault/service/coins/get", {
      app_code: "ice",
    }).then((res) => {
      const { data } = res;
      if (data.status) {
        const { coins_data } = data;
        setCoinList(coins_data);
      }
    });
  }, []);

  const [appList, setAppList] = useState([]);
  useEffect(() => {
    Axios.get("https://comms.globalxchange.io/gxb/apps/get").then((res) => {
      const { data } = res;
      if (data.status) {
        const { apps } = data;
        setAppList(apps);
      }
    });
  }, []);

  useEffect(() => {
    if (appSelected) setAppCode(appSelected.app_code);
  }, [appSelected, setAppCode]);

  useEffect(() => {
    if (coinSelected) setCoinSymbol(coinSelected.coinSymbol);
    else setCoinSymbol("");
  }, [coinSelected, setCoinSymbol]);

  const [coinSearch, setCoinSearch] = useState("");
  const [appSearch, setAppSearch] = useState("");

  const showListItems = () => {
    switch (showList) {
      case "coin":
        return (
          <Scrollbars
            className="coinList"
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
            renderView={(props) => <div {...props} className="view" />}
          >
            <img
              src={allImg}
              className={`coin ${coinSelected === ""}`}
              alt=""
              onClick={() => {
                setCoinSelected("");
                setShowList("");
              }}
            />
            {coinList
              .filter(
                (coin) =>
                  coin.coinName
                    .toLowerCase()
                    .includes(coinSearch.toLowerCase()) ||
                  coin.coinSymbol
                    .toLowerCase()
                    .includes(coinSearch.toLowerCase())
              )
              .map((coin) => (
                <img
                  className={`coin ${coinSelected === coin}`}
                  src={coin.coinImage}
                  onClick={() => {
                    setCoinSelected(coin);
                    setShowList("");
                  }}
                  alt=""
                />
              ))}
          </Scrollbars>
        );
      case "app":
        return (
          <Scrollbars
            className="appList"
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
            renderView={(props) => <div {...props} className="view" />}
          >
            <img
              src={allImg}
              className={`coin ${appSelected === ""}`}
              alt=""
              onClick={() => {
                setAppSelected("");
                setShowList("");
              }}
            />
            {appList
              .filter(
                (app) =>
                  app.app_name
                    .toLowerCase()
                    .includes(appSearch.toLowerCase()) ||
                  app.app_code.toLowerCase().includes(appSearch.toLowerCase())
              )
              .map((app) => (
                <div
                  className={`app ${appSelected === app}`}
                  onClick={() => {
                    setAppSelected(app);
                    setShowList("");
                  }}
                >
                  <img
                    src={(app.data && app.data.colouredlogo) || app.app_icon}
                    alt=""
                  />
                  <div className="labelAppWrapper">
                    <div className="labelApp">{app.app_name}</div>
                  </div>
                </div>
              ))}
          </Scrollbars>
        );
      default:
        break;
    }
  };

  const showInputs = () => {
    switch (showList) {
      case "coin":
        return (
          <input
            type="text"
            value={coinSearch}
            onChange={(e) => setCoinSearch(e.target.value)}
            className="searchInp"
            placeholder="Which Coin Do You Want To Filter By?"
          />
        );
      case "app":
        return (
          <input
            type="text"
            value={appSearch}
            onChange={(e) => setAppSearch(e.target.value)}
            className="searchInp"
            placeholder="Which App Do You Want To Filter By?"
          />
        );
      default:
        return (
          <input
            type="text"
            value={emailSearch}
            onChange={(e) => setEmailSearch(e.target.value)}
            className="searchInp"
            placeholder={`${placeholder}${
              appSelected && " Initiated From " + appSelected.app_name
            }${coinSelected && " With " + coinSelected.coinName}`}
          />
        );
    }
  };

  useEffect(() => {
    if (showList === "more") {
      setMoreFilter(true);
    } else {
      setMoreFilter(false);
    }
  }, [showList, setMoreFilter]);

  return (
    <div className="viewingAs">
      {showInputs()}
      <div
        className={`btnType ${showList === "app"}`}
        onClick={() => setShowList((prev) => (prev === "app" ? "" : "app"))}
      >
        {appSelected === "" ? (
          "All Apps"
        ) : (
          <>
            <img
              className="coin"
              src={
                (appSelected.data && appSelected.data.colouredlogo) ||
                appSelected.app_icon
              }
              alt=""
            />{" "}
            {appSelected.app_name}
          </>
        )}
      </div>
      <div
        className={`btnType ${showList === "coin"}`}
        onClick={() => setShowList((prev) => (prev === "coin" ? "" : "coin"))}
      >
        {coinSelected === "" ? (
          "All Coins"
        ) : (
          <>
            <img className="coin" src={coinSelected.coinImage} alt="" />{" "}
            {coinSelected.coinSymbol}
          </>
        )}
      </div>
      <div
        className={`btnType ${showList === "more"}`}
        onClick={() => setShowList((prev) => (prev === "more" ? "" : "more"))}
      >
        More Filters
      </div>
      {showListItems()}
    </div>
  );
}

export default ViweingAsWithdrawals;
