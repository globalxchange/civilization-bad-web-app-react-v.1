import React from 'react'
import { ReactComponent as BoardsIcon } from '../../images/clipIcons/boards.svg'
import { ReactComponent as ListIcon } from '../../images/clipIcons/list.svg'
import styles from './viewSwitcher.module.scss'

function ViewSwitcher({ isList, setIsList }) {
  return (
    <div className={styles.viewSwitcher}>
      <div
        className={`${styles.btSwitchView} ${!isList && styles.true}`}
        onClick={() => setIsList(false)}
      >
        <BoardsIcon />
        <span> Boards</span>
      </div>
      <div
        className={`${styles.btSwitchView} ${isList && styles.true}`}
        onClick={() => setIsList(true)}
      >
        <ListIcon />
        <span> List</span>
      </div>
    </div>
  )
}

export default ViewSwitcher
