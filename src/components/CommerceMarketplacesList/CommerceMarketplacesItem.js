import React, { useRef } from "react";
import OnOutsideClick from "../../utils/OnOutsideClick";

function CommercemarketplacesItem({ commercemarketplaces, onClick, selected }) {
  const btnRef = useRef();
  const outRef = useRef();

  OnOutsideClick(
    btnRef,
    () => {
      try {
        onClick();
      } catch (error) {}
    },
    outRef
  );

  return (
    <div
      className={`listItem ${commercemarketplaces === selected}`}
      ref={outRef}
    >
      <div className="profile">
        <img src={commercemarketplaces.icon} alt="" className="icon" />
        <span className="name">{commercemarketplaces.name}</span>
      </div>
      <span className="count">{commercemarketplaces?.usersCount} Brands</span>
      <div
        className="btnWebsite"
        ref={btnRef}
        onClick={() =>
          window
            .open(
              commercemarketplaces?.additional_data?.website.includes("http")
                ? commercemarketplaces?.additional_data?.website
                : `https://${commercemarketplaces?.additional_data?.website}`,
              "_blank"
            )
            .focus()
        }
      >
        Website
      </div>
    </div>
  );
}

export default CommercemarketplacesItem;
