import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import moment from "moment";
import Scrollbars from "react-custom-scrollbars";

import { FormatCurrency } from "../../utils/FunctionTools";
import { TabsContext } from "../../context/TabsContext";
import { MainContext } from "../../context/MainContext";

function DepositListItem({
  txn,
  uplineBtnAction,
  txnSelected,
  setTxnSelected,
  onAction,
  setToCopy,
}) {
  const { newTab } = useContext(TabsContext);
  const { allApps, rates } = useContext(MainContext);
  const [profile, setProfile] = useState({
    username: "",
    name: "",
    profile_img: "",
  });
  useEffect(() => {
    Axios.post("https://comms.globalxchange.io/get_affiliate_data_no_logs", {
      email: txn.email,
    }).then((res) => {
      const data = res.data[0];
      if (data) {
        setProfile({
          username: data.username,
          name: data.name,
          profile_img: data.profile_img,
        });
      }
    });
  }, [txn]);

  return (
    <>
      <div
        className={`transactionItm ${!txnSelected || txnSelected === txn}`}
        onClick={() => setTxnSelected(txn)}
        key={txn._id}
      >
        <div className="txnHead">
          <div className="banker">
            {allApps && allApps[txn.app_code]?.app_name}
          </div>
          <img
            src={allApps && allApps[txn.app_code]?.app_icon}
            alt=""
            className="bankerLogo"
          />
        </div>
        <div className="txnContent">
          <div className="nameNativeValue">
            <span>{profile.name || profile.username}</span>
            <span>
              {FormatCurrency(txn.buy_amount, txn.coin)} {txn.coin}
            </span>
          </div>
          <div className="emailUsdValue">
            <span>{txn.email}</span>
            <span>${FormatCurrency(txn.buy_amount * rates[txn.coin])} USD</span>
          </div>
          <Scrollbars
            className="btnScrlWrap"
            renderView={(props) => <div {...props} className="btnScrlList" />}
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
            autoHide
          >
            <div className="btnAction">Source Of Funds</div>
            <div className="btnAction" onClick={uplineBtnAction}>
              Uplines
            </div>
            <div className="btnAction">Why Meter</div>
            <div
              className="btnAction"
              onClick={() => {
                try {
                  onAction(txn._id, "credit");
                } catch (e) {}
              }}
            >
              Credit
            </div>
            <div
              className="btnAction"
              onClick={() => {
                try {
                  onAction(txn._id, "update");
                } catch (e) {}
              }}
            >
              Update
            </div>
            <div
              className="btnAction"
              onClick={() => {
                newTab(`/withdrawals/${txn._id}`);
              }}
            >
              Expand
            </div>
            <div className="btnAction" onClick={() => setToCopy(txn)}>
              Copy
            </div>
          </Scrollbars>
        </div>
        <div
          className={`txnFooter ${
            txn.current_step_data && txn.current_step_data.status
          }`}
        >
          <span>{moment(txn.timestamp).format("MMM Do YYYY")}</span>
          <span>{moment(txn.timestamp).format("h:mm:ss A z")}</span>
        </div>
      </div>
    </>
  );
}

export default DepositListItem;
