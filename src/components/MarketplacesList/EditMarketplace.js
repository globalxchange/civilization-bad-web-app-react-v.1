import React, { useContext, useEffect, useState } from "react";
import Axios from "axios";
import JsonWebToken from "jsonwebtoken";
import Scrollbars from "react-custom-scrollbars";
import { useHistory } from "react-router";
import cloudUploadIcon from "../../static/images/postClipArt/cloudUpload.svg";
import LoadingAnim from "../LoadingAnim/LoadingAnim";
import { MainContext } from "../../context/MainContext";
import loadingGif from "../../static/animations/loading.gif";
import WebsiteInput from "../WebsiteInput";

function renameFile(originalFile, newName) {
  return new File([originalFile], newName, {
    type: originalFile.type,
    lastModified: originalFile.lastModified,
  });
}

const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; // secret not to be disclosed anywhere.
const emailDev = "rahulrajsb@outlook.com"; // email of the developer.

function EditMarketplace({ marketplace }) {
  const history = useHistory();
  const { tostShowOn, email, token } = useContext(MainContext);
  const [step, setStep] = useState("");
  const [loading, setLoading] = useState(false);

  // Form Variables
  const [emName, setEmName] = useState("");
  const [icon, setIcon] = useState("");
  const [logo, setLogo] = useState("");
  const [whiteIcon, setWhiteIcon] = useState("");
  const [whiteLogo, setWhiteLogo] = useState("");
  const [coverPhoto, setCoverPhoto] = useState("");
  const [primaryColor, setPrimaryColor] = useState("");
  const [secondaryColor, setSecondaryColor] = useState("");
  const [websiteTitle, setWebsiteTitle] = useState("");
  const [websiteDescription, setWebsiteDescription] = useState("");
  const [desc, setDesc] = useState("");
  const [website, setWebsite] = useState("");

  const [iconLoading, setIconLoading] = useState(false);
  const [logoLoading, setLogoLoading] = useState(false);
  const [whiteIconLoading, setWhiteIconLoading] = useState(false);
  const [whiteLogoLoading, setWhiteLogoLoading] = useState(false);
  const [coverPhotoLoading, setCoverPhotoLoading] = useState(false);

  useEffect(() => {
    setEmName(marketplace.name);
    setIcon(marketplace.icon);
    setCoverPhoto(marketplace.cover_photo);
    setDesc(marketplace.description);
    setLogo(marketplace.additional_data.colouredlogo);
    setWhiteIcon(marketplace.additional_data.whiteicon);
    setWhiteLogo(marketplace.additional_data.whitelogo);
    setPrimaryColor(marketplace.additional_data.colourcode);
    setSecondaryColor(marketplace.additional_data.colourcode2);
    setWebsiteTitle(marketplace.additional_data.websitetitle);
    setWebsiteDescription(marketplace.additional_data.websitedescription);
    setWebsite(marketplace.additional_data.website);
  }, [marketplace]);

  const uploadImage = async (e, setImage, setLoading) => {
    setLoading(true);
    const fileName = `${new Date().getTime()}${e.target.files[0].name.substr(
      e.target.files[0].name.lastIndexOf(".")
    )}`;
    const formData = new FormData();
    const file = renameFile(e.target.files[0], fileName);
    formData.append("files", file);
    const path_inside_brain = "root/";
    const token = JsonWebToken.sign(
      { name: fileName, email: emailDev },
      secret,
      {
        algorithm: "HS512",
        expiresIn: 240,
        issuer: "gxjwtenchs512",
      }
    );
    let { data } = await Axios.post(
      `https://drivetest.globalxchange.io/file/dev-upload-file?email=${emailDev}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
      formData,
      {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      }
    );
    setImage(data.payload.url);
    setLoading(false);
  };

  function getContent() {
    switch (step) {
      case "updateSucces":
        setTimeout(() => {
          history.push("/viral/Endorsement Marketplace");
          window.location.reload();
        }, 2000);
        return (
          <>
            <div className="godsEyeFilterCurrency">
              <div className="labelItm m-auto">
                You Have Successfully Updated {marketplace.name}. You Will Be
                Redirected To The Updated Marketplace List Automatically
              </div>
            </div>
          </>
        );
      default:
        return (
          <Scrollbars
            className="scrollForm"
            renderTrackHorizontal={() => <div />}
            renderThumbHorizontal={() => <div />}
            renderTrackVertical={() => <div />}
            renderThumbVertical={() => <div />}
          >
            <div className="name">Endorsement Market Name</div>
            <div className="inputWrap">
              <input
                type="text"
                className="text"
                placeholder="Enter Name"
                value={emName}
                onChange={(e) => setEmName(e.target.value)}
              />
            </div>
            <div className="name">Describe The Endorsement Market</div>
            <div className="inputWrap">
              <input
                type="text"
                className="text"
                placeholder="Enter Description Here"
                value={desc}
                onChange={(e) => setDesc(e.target.value)}
              />
            </div>
            <div className="name">Upload Branding Material</div>
            <div className="filesUpload">
              <label className="fileInp icon">
                <img
                  className={`${Boolean(icon)}`}
                  src={iconLoading ? loadingGif : icon || cloudUploadIcon}
                  alt=""
                />
                <input
                  type="file"
                  onChange={(e) => {
                    uploadImage(e, setIcon, setIconLoading);
                  }}
                  accept="image/*"
                />
                <div className="text">Colored Icon</div>
                <div className="hovTxt">
                  Upload
                  <br />
                  New
                </div>
              </label>
              <label className="fileInp icon">
                <img
                  className={`white ${Boolean(whiteIcon)}`}
                  src={
                    whiteIconLoading ? loadingGif : whiteIcon || cloudUploadIcon
                  }
                  alt=""
                />
                <input
                  type="file"
                  onChange={(e) => {
                    uploadImage(e, setWhiteIcon, setWhiteIconLoading);
                  }}
                  accept="image/*"
                />
                <div className="text">White Icon</div>
                <div className="hovTxt">
                  Upload
                  <br />
                  New
                </div>
              </label>
              <label className="fileInp cover">
                <img
                  className={`${Boolean(coverPhoto)}`}
                  src={
                    coverPhotoLoading
                      ? loadingGif
                      : coverPhoto || cloudUploadIcon
                  }
                  alt=""
                />
                <input
                  type="file"
                  onChange={(e) => {
                    uploadImage(e, setCoverPhoto, setCoverPhotoLoading);
                  }}
                  accept="image/*"
                />
                <div className="text">Cover Photo</div>
                <div className="hovTxt">
                  Upload
                  <br />
                  New
                </div>
              </label>
            </div>
            <div className="filesUpload last">
              <label className="fileInp fullLogo">
                <img
                  className={`${Boolean(logo)}`}
                  src={logoLoading ? loadingGif : logo || cloudUploadIcon}
                  alt=""
                />
                <input
                  type="file"
                  onChange={(e) => {
                    uploadImage(e, setLogo, setLogoLoading);
                  }}
                  accept="image/*"
                />
                <div className="text">Colored Logo</div>
                <div className="hovTxt">
                  Upload
                  <br />
                  New
                </div>
              </label>
              <label className="fileInp fullLogo">
                <img
                  className={`white ${Boolean(whiteLogo)}`}
                  src={
                    whiteLogoLoading ? loadingGif : whiteLogo || cloudUploadIcon
                  }
                  alt=""
                />
                <input
                  type="file"
                  onChange={(e) => {
                    uploadImage(e, setWhiteLogo, setWhiteLogoLoading);
                  }}
                  accept="image/*"
                />
                <div className="text">White Logo</div>
                <div className="hovTxt">
                  Upload
                  <br />
                  New
                </div>
              </label>
            </div>
            <div className="name">Enter The Website</div>
            <WebsiteInput website={website} setWebsite={setWebsite} />
            <div className="name">Website Title</div>
            <div className="inputWrap">
              <input
                type="text"
                className="text"
                placeholder="Enter Title Here"
                value={websiteTitle}
                onChange={(e) => setWebsiteTitle(e.target.value)}
              />
            </div>
            <div className="name">Website Description</div>
            <div className="inputWrap">
              <input
                type="text"
                className="text"
                placeholder="Enter Description Here"
                value={websiteDescription}
                onChange={(e) => setWebsiteDescription(e.target.value)}
              />
            </div>
            <div className="name">Enter Primary Color Code</div>
            <div className="inputWrap">
              <input
                type="text"
                className="text"
                placeholder="Enter Code Here..."
                value={primaryColor}
                onChange={(e) => setPrimaryColor(e.target.value)}
              />
              <div
                className="color"
                style={{ width: "10%", background: `#${primaryColor}` }}
              />
            </div>
            <div className="name">Enter Secondary Color Code</div>
            <div className="inputWrap">
              <input
                type="text"
                className="text"
                placeholder="Enter Code Here..."
                value={secondaryColor}
                onChange={(e) => setSecondaryColor(e.target.value)}
              />
              <div
                className="color"
                style={{ width: "10%", background: `#${secondaryColor}` }}
              />
            </div>
            <div className="space"></div>
          </Scrollbars>
        );
    }
  }

  function addEndorsement() {
    setLoading(true);
    Axios.post("https://comms.globalxchange.io/gxb/product/marketplace/edit", {
      email: email,
      token: token,
      marketplace_id: marketplace.marketplace_id,
      update_data: {
        name: emName === marketplace?.name ? undefined : emName,
        description: desc === marketplace.description ? undefined : desc,
        icon: icon === marketplace.icon ? undefined : icon,
        cover_photo:
          coverPhoto === marketplace.cover_photo ? undefined : coverPhoto,
        additional_data:
          website !== marketplace.additional_data.website ||
          websiteTitle !== marketplace.additional_data.websitetitle ||
          websiteDescription !==
            marketplace.additional_data.websitedescription ||
          primaryColor !== marketplace.additional_data.colourcode ||
          secondaryColor !== marketplace.additional_data.colourcode2 ||
          logo !== marketplace.additional_data.colouredlogo ||
          whiteLogo !== marketplace.additional_data.whitelogo ||
          whiteIcon !== marketplace.additional_data.whiteicon
            ? {
                website: website,
                websitetitle: websiteTitle,
                websitedescription: websiteDescription,
                colourcode: primaryColor,
                colourcode2: secondaryColor,
                colouredlogo: logo,
                whitelogo: whiteLogo,
                whiteicon: whiteIcon,
              }
            : undefined,
      },
    })
      .then(({ data }) => {
        if (data.status) {
          tostShowOn("Marketplace Updated");
          setStep("updateSucces");
        } else {
          tostShowOn(data.message);
        }
      })
      .finally(() => setLoading(false));
  }

  function validate() {
    if (
      emName &&
      icon &&
      coverPhoto &&
      logo &&
      whiteIcon &&
      whiteLogo &&
      primaryColor &&
      secondaryColor &&
      websiteTitle &&
      websiteDescription &&
      desc &&
      website
    ) {
      addEndorsement();
    } else {
      tostShowOn("All Fields Are Mandatory");
    }
  }

  return (
    <>
      <div className="newConglomerate">{getContent()}</div>
      <div className="btnSubmit" onClick={validate}>
        Submit Marketplace Edit
      </div>
      {loading && <LoadingAnim />}{" "}
    </>
  );
}

export default EditMarketplace;
