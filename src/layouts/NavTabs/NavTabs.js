import React, { useContext, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";

import { TabsContext } from "../../context/TabsContext";
import assetsLogoFull from "../../static/images/logos/assetsLogoFull.svg";
import engage from "../../static/images/sidebarIcons/engage.svg";
import assetsLogoText from "../../static/images/sidebarIcons/society.svg";
import bankersLogo from "../../static/images/sidebarIcons/bankersLogo.svg";

function NavTabs() {
  const history = useHistory();
  const { pathname } = useLocation();
  const { tabs, setTabs, tabIndex, setTabIndex } = useContext(TabsContext);
  useEffect(() => {
    setTabs((tabs) => {
      let tempArr = tabs;
      tempArr[tabIndex] = pathname;
      return tempArr;
    });
  }, [pathname, setTabs, tabIndex]);
  function newTab() {
    setTabIndex(tabs.length);
    setTabs((tabs) => [...tabs, tabs[tabIndex]]);
  }
  function removeTab(i) {
    setTabs((tabs) => tabs.filter((v, index) => index !== i));
    let indexTemp = tabIndex - 1 || 0;
    if (indexTemp < 1) indexTemp = 0;
    history.push(tabs[indexTemp]);
    setTabIndex(indexTemp);
  }
  function getNavHead(tab) {
    switch (true) {
      case /\/AssetsIo/.test(tab):
        return (
          <>
            <div className="navLogoText">
              <img src={assetsLogoFull} className="navLogo" alt="" />
            </div>
            <div className="divider" />
          </>
        );
      case /\/liquidity/.test(tab):
        return (
          <>
            <div className="navLogoText">
              <img src={bankersLogo} className="navLogo" alt="" />
              <span>Liquidity</span>
            </div>
            <div className="divider" />
          </>
        );
      case /\/content/.test(tab):
        return (
          <>
            <div className="navLogoText">
              <img src={engage} className="navLogo" alt="" />
              <span>Engagement</span>
            </div>
            <div className="divider" />
          </>
        );
      case "/" === tab:
        return (
          <>
            <div className="navLogoText">
              <img src={assetsLogoText} className="navLogo" alt="" />
              <span>Society</span>
            </div>
            <div className="divider" />
          </>
        );
      default:
        break;
    }
  }
  return (
    <div className="navList">
      {tabs.map((tab, i) => (
        <div
          key={`${i}-${tab}`}
          className={`navBarWrapper ${i === tabIndex}`}
          onClick={() => {
            setTabIndex(i);
            history.push(tab);
          }}
        >
          {getNavHead(tab)}
          {tabs && tabs.length && tabs.length > 1 && (
            <div
              className="btnClose"
              onClick={() =>
                setTimeout(() => {
                  removeTab(i);
                }, 200)
              }
            />
          )}
        </div>
      ))}
      <div className="addNav" onClick={() => newTab()} />
    </div>
  );
}

export default NavTabs;
